<?php

use Illuminate\Support\Facades\Route;

Route::get("/clear",function(){
    Artisan::call("config:clear");
    Artisan::call("cache:clear");
    return 'Done';
});

Route::get('/',                    'Auth\LoginController@login');
Route::get('sign-up',              'Auth\LoginController@signUp');
Route::post('store-user',              'Auth\LoginController@store_user');
Route::post('checkAuthentication', 'Auth\LoginController@authenticate');
Route::get('logout',               'Auth\LoginController@logout');

Route::group(['middleware'=>['auth'],'namespace'=>'Permission'],function() {

    Route::get('/role-permission', 'RoleController@index')->name("role");
    Route::post('/role', 'RoleController@store');
    Route::get('/role/edit/{role_id}', 'RoleController@edit');
    Route::get('/role/delete/{role_id}', 'RoleController@delete');
    Route::get('/assign-permission/{role_id}', 'RoleController@assignPermission');
    Route::post('/assign-permission/{role_id}', 'RoleController@assignPermission');

    Route::get('/module', 'ModuleController@index')->name("module");
    Route::post('/module/add_parent', 'ModuleController@add_parent');
    Route::get('/module/get_subparent', 'ModuleController@get_subparent');
    Route::post('/module/add', 'ModuleController@add');
    Route::post('/module/control', 'ModuleController@control');
    Route::get('/module/edit/{id?}/{cat_id?}/{msg?}', 'ModuleController@edit');
    Route::post('/module/edit/{id?}/{cat_id?}/{msg?}', 'ModuleController@edit');
    Route::get('/module/delete/{id?}/{cat_id?}/{msg?}', 'ModuleController@delete');
    Route::post('/module/moduleUpdate/', 'ModuleController@moduleUpdate');
   
});

Route::group([ 'middleware' => 'checkAuthentication'], function(){

    Route::get('/dashboard',     'AdminController@index');
    Route::get('/userDashboard', 'AdminController@userDashboard');

    Route::prefix('/careers')->name('careers.')->group(function(){
        Route::get('/', 'CareerController@index')->name('index');
        Route::get('/create', 'CareerController@create')->name('create');
        Route::get('/circularDetails', 'CareerController@circularDetails')->name('circularDetails');
        Route::post('/store', 'CareerController@store')->name('store');
        Route::get('edit/{id}', 'CareerController@edit')->name('edit');
        Route::post('update/{id}', 'CareerController@update')->name('update');
        Route::get('destroy/{id}', 'CareerController@destroy')->name('destroy');
    });

/*---Role---*/
    Route::get('addRole',              'RoleController@createRole');
    Route::post('saveRole',            'RoleController@storeRole');
    Route::get('editRole/{role_id}',   'RoleController@editRole');
    Route::post('updateRole',          'RoleController@updateRole');
    Route::get('deleteRole/{role_id}', 'RoleController@deleteRole'); 


/*---Module---*/
    Route::get('addModule',              'ModuleController@createModule');
    Route::post('saveModule',            'ModuleController@storeModule');
    Route::get('editModule/{module_id}', 'ModuleController@editModule');
    Route::post('updateModule',          'ModuleController@updateModule');

/*---Role Permission---*/
    Route::get('addPermission',                    'RolePermissionController@createPermission');
    Route::get('managePermission',                 'RolePermissionController@managePermission');
    Route::post('savePermission',                  'RolePermissionController@storePermission');
    Route::get('editPermission/{permission_id}',   'RolePermissionController@editPermission');
    Route::post('updatePermission',                'RolePermissionController@updatePermission');
    Route::get('deletePermission/{permission_id}', 'RolePermissionController@deletePermission');
    Route::get('detailsPermission',                'RolePermissionController@viewDetailsPermission');


/*---User info---*/
    Route::get('manageUser',               'UserController@manageUser');
    Route::get('addUser',                  'UserController@createUser');
    Route::post('saveUser',                'UserController@storeUser');
    Route::get('doUserInactive/{user_id}', 'UserController@inactiveUser');
    Route::get('doUserActive/{user_id}',   'UserController@activeUser');
    Route::get('editProfile/{user_id}',    'UserController@editUser');
    Route::post('updateUser',              'UserController@updateUser');
    Route::get('changePassword/{user_id}', 'UserController@changeUserPassword');
    Route::post('updatePassword',          'UserController@updatePassword');

});