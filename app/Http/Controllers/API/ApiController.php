<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function jobPost(){

        $job_post = Career::latest()->get();
        return response()->json([
            'status'=>true,
            'message'=>['Job Post List'],
            'data'=>$job_post,
            ],200);
    }

    public function getUser(){
        
        $user_list = User::latest()->get();
        return response()->json([
            'status'=>true,
            'message'=>['All Users List'],
            'data'=>$user_list,
            ],200);
    }


}