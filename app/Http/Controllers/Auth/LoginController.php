<?php

namespace App\Http\Controllers\Auth;

use DB;
use Member;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(){
        if(Session::get('loggedData')){
            return redirect('/dashboard');
        }else{    
        	$this->data['headline'] = 'LOG IN';
        	return view('admin.login.login', $this->data);
        }
    }

    public function signUp(){
           
        $this->data['headline'] = 'Sign Up Here';
        return view('admin.login.attendence', $this->data);
        
    }

    public function store_user(Request $request){

       $this->validate($request,[
            'name'=>'required',
            'role_id'=>'required',
            'email'=>'required',
            'password'=>'required',
            'confirm_password'=>'required',
        ]);

       $password = $request->password;
       $confirm_password = $request->confirm_password;

       if($password == $confirm_password){

            $result=DB::table('users')
                ->insert([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'password'=>Hash::make($request->password),
                    'role_id'=>$request->role_id,
                ]);

            if($result){
                
                setMessage("message","success","Saved successfully ! Please Log In"); 
                return redirect('/');
            }else{

                setMessage("message","danger","Failed to add !!!"); 
                return redirect('sign-up');
            }

        }else{
            
            setMessage("message","danger","Password and Confirm Password does not matched !!!"); 
            return redirect('sign-up');
        }

    }//storeUser

    public function authenticate(Request $request){

    	$data =  $request->only(['email', 'password']);

    	if(Auth::attempt([ 'email'=>$data['email'], 'password'=>$data['password'], 'status'=>1 ])){
            
            Session::put('loggedData',Auth::user());
    		return redirect()->intended('/dashboard');

    	}else{

            return redirect()->intended('/')->withErrors(['Invaild User email or password']);
        }
    	
    }//authenticate

    public function logout(){
        Auth::logout();
        Session::forget('loggedData');
        return redirect()->intended('/')->withErrors(['You have successfully logged out']);
    }


}//LoginController