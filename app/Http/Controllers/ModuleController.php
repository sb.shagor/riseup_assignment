<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
use Session;

class ModuleController extends Controller
{
    public function createModule(){
    	$modules = Module::all();
        return view('admin.module.moduleManage', ['allModule'=>$modules]);
    }

    public function storeModule(Request $request){

    	$this->validate($request,[
    		'module_name'=>'required',
    	]);

    	$module = new Module();
    	$module->module_name = $request->module_name;
    	$module->save();
    	return redirect('addModule')->with('message','Module has been saved !!!');
    }

    public function editModule($module_id){
    	$modules = Module::all();
    	$moduleById = Module::find($module_id);
        return view('admin.module.editModule', ['allModule'=>$modules, 'selected_info'=>$moduleById]);
    }

     public function updateModule(Request $request){
    	$module = Module::find($request->id);
    	$module->module_name = $request->module_name;
    	$module->save();
    	return redirect('addModule')->with('message','Module has been updated !!!');
    }


}//ModuleController