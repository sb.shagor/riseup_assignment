<?php

namespace App\Http\Controllers;

use DB;
use Session;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Services\MemberService;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function manageUser(){
    	$roles = Role::all();
    	$users = DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id', 'LEFT')
            ->select('users.*', 'roles.role_name')
            ->get();
    	return view('admin.user.manageUser',['allUsers'=>$users, 'allRoles'=>$roles]);
    }

    public function createUser(){
        $roles = Role::all();
        return view('admin.user.createUser',['allRoles'=>$roles]);
    }

    public function storeUser(Request $request){
       $this->validate($request,[
            'name'=>'required',
            'role_id'=>'required',
            'email'=>'required',
            'password'=>'required',
            'confirm_password'=>'required',
        ]);

       $password = $request->password;
       $confirm_password = $request->confirm_password;

       if($password == $confirm_password){
            $imageUrl='';
            if(!empty($request->file('image'))){
                $userImg = $request->file('image');
                $name = $userImg->getClientOriginalName();
                $uploadPath = 'userImage/';
                $userImg->move($uploadPath, $name);
                $imageUrl = $uploadPath.$name;
            }
            $result=DB::table('users')
                ->insert([
                    'name'=>$request->name,
                    'role_id'=>$request->role_id,
                    'email'=>$request->email,
                    'password'=>Hash::make($request->password),
                    'image'=>($imageUrl) ? $imageUrl : '',
                ]);

            if($result){
                setMessage("message","success","User has been save successfully !!!"); 
                return redirect('manageUser');
            }else{
                setMessage("message","danger","Failed to add !!!"); 
                return redirect('manageUser');
            }

        }else{
            setMessage("message","danger","Password and Confirm Password does not matched !!!"); 
            return redirect('addUser');
        }

    }//storeUser


    public function editUser($user_id){
    	$userByID = DB::table('users')->find($user_id);
    	return view('admin.user.editUser',['userByID'=>$userByID]);
    }

    public function updateUser(Request $request){

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
        ]);
       
        $userByID = DB::table('users')->find($request->id);
        $userImage = $request->file('image');

        if($userImage){
            $preImg = $userByID->image;
            if(!empty($preImg)){
                unlink($preImg);
            }
            $name = $userImage->getClientOriginalName();
            $uploadPath = 'userImage/';
            $userImage->move($uploadPath, $name);
            $imageUrl = $uploadPath.$name;

            $result = DB::table('users')
                ->where('id',$request->id)
                ->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'image'=>$imageUrl,
                ]);

            if($result){
                setMessage("message",'success',"User has been updated !!!");
                return redirect('editProfile/'.$request->id);
            }else{
                setMessage("message",'danger',"Failed to update !!!");
                return redirect('editProfile/'.$request->id);
            }
            
        }else{

            $result = DB::table('users')
                ->where('id',$request->id)
                ->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                ]);

            if($result){
                setMessage("message",'success',"User has been updated !!!");
                return redirect('editProfile/'.$request->id);
            }else{
                setMessage("message",'danger',"Failed to update !!!");
                return redirect('editProfile/'.$request->id);
            }
        }

        
    }//updateUser

    public function inactiveUser($user_id){

    	$query = DB::table('users')
              ->where('id', $user_id)
              ->update(['status' => 0]);
        if($query){
            setMessage("message",'success',"This Member has been Inactive now !!!");
    		return redirect('manageUser');
	    }else{

            setMessage("message",'danger',"Failed to Inactive operation !!!");
    		return redirect('manageUser');
	    }
    }

    public function activeUser($user_id){

    	$query = DB::table('users')
              ->where('id', $user_id)
              ->update(['status' => 1]);

        if($query){
            setMessage("message",'success',"This Member has been Active now !!!");
            return redirect('manageUser');
        }else{

            setMessage("message",'danger',"Failed to Active operation !!!");
            return redirect('manageUser');
        }
    }

//========================== User Password Update =============================//

    public function changeUserPassword($user_id){
        $userByID = DB::table('users')->find($user_id);
        return view('admin.user.changePassword',['userByID'=>$userByID]);
    }


    public function updatePassword(Request $request){

        $this->validate($request,[
            'old_password'=>'required',
            'password'=>'required',
            'confirm_password'=>'required',
        ]);

        $userByID = DB::table('users')->find($request->id);
        $userPassword = $userByID->password;

        if (Hash::check($request->input('old_password'), $userPassword)) {
            
            $password = $request->password;
            $confirm_password = $request->confirm_password;

            if($password == $confirm_password){

                $result = DB::table('users')
                ->where('id',$request->id)
                ->update([
                    'password'=>Hash::make($request->password),
                ]);

                if($result){
                    setMessage("message",'success',"User Password has been updated !!! Login Again with New Password !!!"); 
                    return redirect('logout');
                }else{
                    setMessage("message",'danger',"Failed to update !!!"); 
                    return redirect('changePassword/'.$request->id);
                }

            }else{
                setMessage("message",'danger',"Password and Confirm Password does not match !!!");
                return redirect('changePassword/'.$request->id);

            }

        }else{
            setMessage("message",'danger',"Old Password does not match !!!");
            return redirect('changePassword/'.$request->id);
        }


    }//updatePassword


}//UserController