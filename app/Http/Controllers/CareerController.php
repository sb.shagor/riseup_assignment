<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    public function index(){
        $data['get_all'] = Career::get();
        return view('admin.career.view', $data);
    }

    public function create(){
        $data['add'] = TRUE;
        return view('admin.career.add', $data);
    }

    public function store(Request $request){

        $this->validate($request, [          
            'title' => 'required',               
            'description' => 'required',               
            'status' => 'required',                     
            'file' => 'required',                     
        ]);

        try {
            $career = new Career;
            $career->title = $request->title;
            $career->description = $request->description;
            $career->status = $request->status;
            $career->created_by = Auth::id();
            if(!empty($request->file('file'))){
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'career-'.time().'.'.$ext[1];
                $uploadPath = 'career/';
                $File->move($uploadPath, $finalName);
                $career->file = $uploadPath.$finalName;
            }
            $career->save();
             
            setMessage("message","success","Successful");
            return redirect(route('careers.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return redirect(route('careers.index'));
        }
    }

    public function edit($id){
        $data['edit'] = TRUE;
        $data['single'] = Career::findOrFail($id);
        return view('admin.career.add', $data);
    }


    public function update(Request $request, $id){

        $this->validate($request, [          
            'title' => 'required',               
            'description' => 'required',                    
            'status' => 'required',                    
        ]);

        try {
            $career = Career::findOrFail($id);
            $career->title = $request->title;
            $career->description = $request->description;
            $career->status = $request->status;
            $career->updated_by = Auth::id();
            if ($request->file != null) {

                $path = $career->file;
                if (file_exists($path) and $career->file !=null) {
                    unlink($path);
                }
                $File = $request->file('file');
                $name = $File->getClientOriginalName();
                $ext = explode('.',$name);
                $finalName = 'career-'.time().'.'.$ext[1];
                $uploadPath = 'career/';
                $File->move($uploadPath, $finalName);
                $career->file = $uploadPath.$finalName;
            }
            $career->save();

            setMessage("message","success","Successful");
            return redirect(route('careers.index'));

        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

    public function destroy($id){

        try {
            $career = Career::findOrFail($id);
            $path = $career->file;
            if (file_exists($path) and $career->file != null) {
                unlink($path);
            }
            $career->delete();
            setMessage("message","success","Successful");
            return back();
        }catch (\Exception $e) {
            $err_message = \Lang::get($e->getMessage());
            setMessage("message","danger","Failed");
            return back();
        }
    }

    public function circularDetails(){
        $id = request()->input("id");
        $get_info = Career::findOrFail($id);
        return view('admin.career.details',['selected_info'=>$get_info]);
    }


}//CareerController
