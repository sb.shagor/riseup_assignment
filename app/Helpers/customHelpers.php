<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

/* role static id*/
define('ADMIN', '1');
define('GU', '2');

/* Permission */
define('ADD', 'can_add');
define('EDIT', 'can_edit');
define('VIEW', 'can_view');
define('DELETE', 'can_delete');

if (!function_exists('logged_in_user_id')) {

    function logged_in_user_id(){

        $logged_in_id = '';
        $logged_in_id = Auth::user()->id;
        return $logged_in_id;
    }
}

if (!function_exists('logged_in_user_name')) {

    function logged_in_user_name(){

        $logged_in_name = '';
        $logged_in_name = Auth::user()->name;
        return $logged_in_name;
    }
}

if (!function_exists('logged_in_role_id')) {

    function logged_in_role_id(){

        $logged_in_role_id = 0;
        if (Auth::user()->role_id) :
            $logged_in_role_id = Auth::user()->role_id;
        endif;
        return $logged_in_role_id;
    }
}

if (!function_exists('hasPermission')) {

    function hasPermission($module,$permission) {
        $module=trim($module);
        $role_id=Auth::user()->role->id;
        $getrole=\App\Role::find($role_id);
        if($getrole->role_name=="Admin")
        {
            return true;
        }
        $count = DB::table('permission_categories as PC')
            ->leftJoin('role_permissions as RP', 'PC.id', '=', 'RP.permission_category_id')
            ->where('PC.short_code', '=',$module)
            ->where('RP.role_id', '=',$role_id)
            ->where('RP.'.$permission, '=',1)
            ->select('PC.short_code', 'RP.*')
            ->get()->count();
        if($count>0){

            return true;
        }
        else{

            return false;
        }
    }

}

if (!function_exists('checkPermission')) {
    function checkPermission($module,$permission)
    {
        if(!hasPermission($module,$permission))
        {
            setMessage("msg","warning","Permission Denied!");
            return redirect()->to('/dashboard')->send();
            exit;
        }
    }
}

if (!function_exists('logged_in_role_name')) {

    function logged_in_role_name() {
        $logged_in_role_name = '';
       if (Auth::user()->id && Auth::user()->role->id):
            $logged_in_role_name=Auth::user()->role->role_name;
        endif;
        return $logged_in_role_name;
    }
}

if (!function_exists('is_super_admin')) {

    function is_super_admin() {
        if(logged_in_role_name() === 'Admin'):
            return true;
        else:
            return false;
        endif;
    }
}

if (!function_exists('hasActive')) {

    function hasActive($module) {
        $module=trim($module);
        $role_id=Auth::user()->role_id;
        $getrole=\App\Role::find($role_id);
        if($getrole->name=="Admin")
        {
            return true;
        }
        $count=DB::table("permission_groups")
            ->where("short_code","=",$module)
            ->where("is_active","=",1)
            ->get()->first();
        if(isset($count)){
            return true;
        }
        else{
            return false;
        }
    }

}

if (!function_exists('setMessage')) {

    function setMessage($key, $class, $message){

        session()->flash($key, $message);
        session()->flash("class", $class);
        return true;
    }
}

if (!function_exists('debug_r')) {

    function debug_r($value){

        echo "<pre>";
        print_r($value);
        echo "</pre>";
        die();
    }
}

if (!function_exists('debug_v')) {

    function debug_v($value){

        echo "<pre>";
        var_dump($value);
        echo "</pre>";
        die();
    }
}

function limit_words($string, $wordsreturned) {
    
    $string = strip_tags($string); // Remove html tag
    $retval = $string; //   Just in case of a problem
    $array = explode(" ", $string);
    /*  Already short enough, return the whole thing */
    if (count($array) <= $wordsreturned) {
        $retval = $string;
    }
    /*  Need to chop of some words */ 
    else {
        array_splice($array, $wordsreturned);
        $retval = implode(" ", $array) . " ......";
    }
    return $retval;
    //echo read_more('word' , 'number');
}
