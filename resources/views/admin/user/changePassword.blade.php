@extends('admin.layout.default')
@section('title')
    Update Your Password
@endsection
@section('content')

    <div class="main-panel">     
        <div class="page-header">
            <h3 class="page-title">Update Your Password</h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Forms</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                </ol>
            </nav>
        </div>   
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">

                            @if(Session::has('message'))
                                <div class="alert alert-{{Session::get('class')}}">
                                {!! Session::get("message") !!}</div>
                            @endif

                            <form action="{{ url('updatePassword') }}" method="post" enctype="multipart/form-data" class="forms-sample">
                            @csrf
                                
                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label"> Old Password <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" name="old_password" value="" class="form-control" placeholder="Enter Old Password" required>
                                        <span class="text-danger">{{ $errors->has('old_password') ? $errors->first('old_password') : '' }}</span>

                                        <input type="hidden" name="id" value="{{ $userByID->id }}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label"> New Password <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" id="password" value="" class="form-control" placeholder="Enter New Password" required>
                                        <span class="text-danger">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label"> Confirm Password <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" name="confirm_password" id="confirm_password" value="" class="form-control" placeholder="Enter Confirm Password" required onkeyup="checkPass(); return false;">
                                        <span class="text-danger">{{ $errors->has('confirm_password') ? $errors->first('confirm_password') : '' }}</span>
                                        <span id="confirmMessage" class="confirmMessage"></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function checkPass(){
            var password = document.getElementById('password');
            var confirm_password = document.getElementById('confirm_password');
            var message = document.getElementById('confirmMessage');
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            if(password.value == confirm_password.value){
                confirm_password.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Password Match!"
            }else{
                confirm_password.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Password Does Not Match!"
            }
        }
    
    </script>

@endsection