@extends('admin.layout.default')
@section('title')
    Update Your Profile
@endsection
@section('content')

    <div class="main-panel">     
        <div class="page-header">
            <h3 class="page-title">Update Your Profile</h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Forms</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                </ol>
            </nav>
        </div>   
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                        
                            @if(Session::has('message'))
                                <div class="alert alert-{{Session::get('class')}}">
                                {!! Session::get("message") !!}</div>
                            @endif

                            <form action="{{ url('updateUser') }}" method="post" enctype="multipart/form-data" class="forms-sample">
                            @csrf
                                
                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label"> Name <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="name" value="{{ $userByID->name }}" class="form-control" placeholder="Enter Name" required>
                                        <span class="text-danger">{{ $errors->has('name') ? $errors->first('name') : '' }}</span>

                                        <input type="hidden" name="id" value="{{ $userByID->id }}" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label"> Email <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" name="email" value="{{ $userByID->email }}" class="form-control" placeholder="Enter email" required>
                                        <span class="text-danger">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Existing Image</label>
                                    <div class="col-sm-9">
                                        <img src="{{ asset($userByID->image) }}" style="height: 60px;width: 100px;"/>
                                    </div>
                                </div>
                               
                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Upload New Image</label>
                                    <div class="col-sm-9">
                                        <input name="image" type="file" class="form-control">
                                        (<code>JPG PNG MAX 1024 KB</code>)
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary mr-2">Update</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection