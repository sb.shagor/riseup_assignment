@extends('admin.layout.default')
@section('title')
    Manage User
@endsection
@section('content')

    <div class="main-panel">
        <div class="page-header">
            <h3 class="page-title">All User's List</h3>
            <a class="nav-link" href="{{ url('addUser') }}">
                <span class="btn btn-primary">+ Create New</span>
            </a>
        </div>
        <div class="content-wrapper">
            <div class="card">
                <div class="card-body">

                    @if(Session::has('message'))
                        <div class="alert alert-{{Session::get('class')}}">
                        {!! Session::get("message") !!}</div>
                    @endif
                    
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>SL.</th>
                                            <th>Name</th>
                                            <th>Role Name</th>
                                            <th>User Email</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($allUsers as $key => $value)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $value->name }}</td>
                                            <td>{{ $value->role_name }}</td>
                                            <td>{{ $value->email }}</td>
                                            <td>
                                                @if(!empty($value->image))
                                                    <img src="{{ asset($value->image) }}" style="height: 60px;width: 60px;"/>
                                                @else
                                                    <img src="{{ asset('noimage.jpg') }}" style="height: 60px;width: 60px;"/>
                                                @endif
                                            </td>
                                            <td>{{ $value->status==1 ? 'Active' : 'Inactive' }}
                                            </td>
                                            <td>

                                                <?php
                                                    if($value->status==1){ ?>

                                                        <a onclick="return confirm('Are You Sure?')" href="{{ url('doUserInactive/'.$value->id) }}" title="" class="text-success btn btn-default  btn-xs  waves-effect tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Active" id=""><i class="fa fa-check-circle"></i></a>

                                                    <?php }else{ ?>

                                                        <a onclick="return confirm('Are You Sure?')" href="{{ url('doUserActive/'.$value->id) }}" title="" class="text-danger btn btn-default  btn-xs  waves-effect tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Inactive" id=""><i class="fa fa-check-circle"></i></a>

                                                <?php } ?>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection