
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    @php 
                      $userID = logged_in_user_id();
                      $user = DB::table('users')->where('id', $userID)->first();
                    @endphp
                    @if($user->image == NULL)
                      {{ Str::substr(logged_in_user_name(), 0,1 ) }}
                    @else
                      <img src="{{ asset($user->image) }}" alt="{{ logged_in_user_name() }}"/>
                    @endif
                </div>
                <div class="profile-name">
                    <p class="name">Welcome</p>
                    <p class="designation">{{ logged_in_user_name() }}</p>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('/') }}">
                <i class="fa fa-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li> 

        @if (hasActive("career") &&hasPermission("career", VIEW))
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#career-layouts" aria-expanded="false" aria-controls="career-layouts">
                    <i class="fa fa-users menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Manage Career</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="career-layouts">
                    <ul class="nav flex-column sub-menu">
                        @if(hasPermission("career", ADD))
                            <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ route('careers.create') }}">Add New</a></li>
                        @endif
                        <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ route('careers.index') }}">View List</a></li>
                    </ul>
                </div>
            </li>
        @endif

        @if(is_super_admin())
            <li class="nav-item">
                <a class="nav-link" data-toggle="collapse" href="#Administrator-layouts" aria-expanded="false" aria-controls="Administrator-layouts">
                    <i class="fa fa-users menu-icon" aria-hidden="true"></i>
                    <span class="menu-title">Administrator</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="Administrator-layouts">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ url('manageUser') }}">Manage User</a></li>
                        <!--<li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ url('module') }}">Module</a></li>-->
                        <li class="nav-item d-none d-lg-block"> <a class="nav-link" href="{{ url('role-permission') }}">Role Permission</a></li>
                    </ul>
                </div>
            </li>
        @endif

    </ul>
</nav>