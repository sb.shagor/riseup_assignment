<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row default-layout-navbar">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="#" ><span style="color: #fff !important;">JOB PORTAL</span></a>
        <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('admin') }}/images/minilogo.png" alt="logo"/></a>
    </div>
    
    <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="fas fa-bars"></span>
        </button>
        
        <ul class="navbar-nav navbar-nav-right">
            
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                  <div class="image-icon">
                    @php 
                      $userID = logged_in_user_id();
                      $user = DB::table('users')->where('id', $userID)->first();
                    @endphp
                    @if($user->image == NULL)
                      {{ Str::substr(logged_in_user_name(), 0,1 ) }}
                    @else
                      <img src="{{ asset($user->image) }}" alt="{{ logged_in_user_name() }}"/>
                    @endif
                  </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                  <a class="dropdown-item" href="{{ url('editProfile/'.$userID) }}">
                    <i class="fas fa-cog text-primary"></i>
                    Profile
                  </a>

                  <a class="dropdown-item" href="{{ url('changePassword/'.$userID) }}">
                    <i class="fas fa-key text-primary"></i>
                    Change Password
                  </a>

                  <div class="dropdown-divider"></div>
                  <a href="{{ URL::to('logout') }}" class="dropdown-item">
                    <i class="fas fa-power-off text-primary"></i>
                    Logout
                  </a>
                </div>
            </li>
            
        </ul>
        
    </div>
</nav>