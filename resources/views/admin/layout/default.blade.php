<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('title')</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{ asset('admin') }}/vendors/iconfonts/font-awesome/css/all.min.css">
        <link rel="stylesheet" href="{{ asset('admin') }}/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="{{ asset('admin') }}/vendors/css/vendor.bundle.addons.css">
        <!-- endinject -->
        <link rel="stylesheet" href="{{ asset('admin') }}/css/style.css">
        <!-- endinject --> 
        <link rel="stylesheet" href="{{ asset('admin') }}/css/jquery-ui.css">

        {{-- Bootstrap live search --}}
        <link rel="stylesheet"href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>
        
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body>
        <div class="container-scroller">
            <!-- header -->
            @include('admin.layout.header')
            <!-- //header -->
            <div class="container-fluid page-body-wrapper">
                <!-- sidebar -->
                @include('admin.layout.sidebar')
                <!-- //sidebar -->
                <!-- main panel -->
                @yield('content')
                <!-- main-panel ends -->
            </div>
        </div>
        <!-- container-scroller -->
        <!-- jQuery  -->
        <!-- <script src="{{ asset('admin') }}/js/jquery.min.js"></script> -->
        <script src="{{ asset('admin') }}/js/bootstrap.min.js"></script>
        <!-- plugins:js -->
        <script src="{{ asset('admin') }}/vendors/js/vendor.bundle.base.js"></script>
        <script src="{{ asset('admin') }}/vendors/js/vendor.bundle.addons.js"></script>
        <!-- endinject -->
        <script src="{{ asset('admin') }}/js/off-canvas.js"></script>
        <script src="{{ asset('admin') }}/js/hoverable-collapse.js"></script>
        <script src="{{ asset('admin') }}/js/misc.js"></script>
        <script src="{{ asset('admin') }}/js/settings.js"></script>
        <script src="{{ asset('admin') }}/js/todolist.js"></script>
        <!-- endinject -->
        <!-- Custom js for this page-->
        <script src="{{ asset('admin') }}/js/dashboard.js"></script>
        <script src="{{ asset('admin') }}/js/data-table.js"></script>
        <script src="{{ asset('admin') }}/js/file-upload.js"></script>
        <script src="{{ asset('admin') }}/js/typeahead.js"></script>
        <script src="{{ asset('admin') }}/js/select2.js"></script>
        <script src="{{ asset('admin') }}/js/tabs.js"></script>
        <!-- End custom js for this page-->
    </body>
</html>