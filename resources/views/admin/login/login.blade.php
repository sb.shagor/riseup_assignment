<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Portal | Login</title>
    <!-- Bootstrap -->
    <link href="{{ asset('login/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('login/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin') }}/vendors/iconfonts/font-awesome/css/all.min.css">
  </head>
  <style type="text/css">
    i.fa.fa-eye {
        position: relative;
        top: -26px;
        left: 200px;
    }
  </style>
  <body>
    <div class="container-fluid">
        <div class="row">
          <div class="banner" style="background: #2D3037 url('login/homebg.png') repeat-x center top;">
            <div class="banner_overlay hidden-xs"><span>Job Portal | Login Panel</span></div>
            <div class="banner_overlay visible-xs"><span>Job Portal | Login Panel</span></div>
          </div>
        </div>
        
        <div class="row login_form_area">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
              <div class="row login_bg">
                <div class="col-md-10">
                  @if(Session::has('message'))
                    <div class="alert alert-block alert-{{Session::get("class")}}">
                      <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                      </button>
                      <i class="ace-icon fa fa-check green"></i>
                      {{ Session::get("message") }}
                    </div>
                  @endif
                </div>

                <div class="col-lg-5 col-md-6">
                    <img src="{{ asset('login/man.png') }}" class="img-responsive hidden-xs center-block">
                </div>

                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12" style="margin-top:35px">
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-block alert-info">
                        <button type="button" class="close" data-dismiss="alert">
                          <i class="ace-icon fa fa-times"></i>
                        </button>
                        <i class="ace-icon fa fa-check green"></i>
                        {{ $error }}
                      </div>
                    @endforeach
                  @endif
                  <form name="login_panel" id="login_panel" action="{{ url('checkAuthentication') }}" method="post">
										@csrf
                      <div class="form-group">
                        <label for="username" class="col-sm-3 hidden-md control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="">
                        </div>
                        @if ($errors->has('email'))
										      <span class="invalid-feedback" role="alert">
										        <strong>{{ $errors->first('email') }}</strong>
										      </span>
										    @endif
                      </div><br><br>
                      <div class="form-group">
                        <label for="password" class="col-sm-3 hidden-md control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="password" name="password" placeholder="******"><a href="javascript:void(0)" id="password_text"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        </div>
                      </div><br><br>
                      @if ($errors->has('password'))
									      <span class="invalid-feedback" role="alert">
									        <strong>{{ $errors->first('password') }}</strong>
									      </span>
									    @endif
                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                          <button type="submit" class="btn btn-default" name="login">Sign in</button>
                        </div>
                      </div>
                      
                    </form>
                </div>                
              </div>
          </div>
        </div>
    </div>

    <!-- footer -->
    <footer class="footer">
      <div class="container-fluid">
        <span class="text-muted">
          <strong>Copyright &copy; <?= date('Y'); ?> New Here?&nbsp;&nbsp;<a class="text-danger" href="<?= url('sign-up'); ?>"><strong >Create Account.</strong></a>
        </span>
        <span class="pull-right"><b>Powered by</b> <a href="#">Solaman Badsha</a></span>
      </div>
    </footer>

  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
 
  <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function(){
            $("#password_text").on("click",function(){
                var x = document.getElementById("password");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            });
        });
    </script>

  </body>
</html>