<!DOCTYPE html>
<html lang="en">
  <head>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Portal | Sign Up</title>
    <!-- Bootstrap -->
    <link href="{{ asset('login/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('login/css/style.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
          <div class="banner" style="background: #2D3037 url('login/homebg.png') repeat-x center top;">
            <div class="banner_overlay hidden-xs"><span>Job Portal | Sign Up</span></div>
            <div class="banner_overlay visible-xs"><span>Job Portal | Sign Up</span></div>
          </div>
        </div>
       
        <div class="row login_form_area">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-10 col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-xs-offset-1">
              <div class="row login_bg">
                <div class="col-md-10">
                  @if(Session::has('message'))
                    <div class="alert alert-block alert-{{Session::get("class")}}">
                      <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                      </button>
                      <i class="ace-icon fa fa-check green"></i>
                      {{ Session::get("message") }}
                    </div>
                  @endif
                </div>

                <div class="col-lg-4 col-md-6">
                    <img src="{{ asset('login/man.png') }}" class="img-responsive hidden-xs center-block">
                </div>

                <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12" style="margin-top:35px">
                  <form name="login_panel" id="login_panel" action="{{ url('store-user') }}" method="post">
                    @csrf

                      <div class="form-group">
                        <label for="username" class="col-sm-3 hidden-md control-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" required>
                          @if ($errors->has('name'))
                            <span class="invalid-feedback text-danger" role="alert">
                              <strong>{{ $errors->first('name') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div><br><br>

                      <div class="form-group">
                        <label for="username" class="col-sm-3 hidden-md control-label">Email</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="" required>
                          @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div><br><br>

                      <div class="form-group">
                        <label for="password" class="col-sm-3 hidden-md control-label">Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required>
                          @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div><br><br>

                      <div class="form-group">
                        <label for="password" class="col-sm-3 hidden-md control-label">Confirm Password</label>
                        <div class="col-sm-9">
                          <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Enter Password" required onkeyup="checkPass(); return false;">
                          @if ($errors->has('confirm_password'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('confirm_password') }}</strong>
                            </span>
                          @endif
                        </div>
                        <span id="confirmMessage" class="confirmMessage"></span>
                      </div><br><br>

                      <div class="form-group">
                        <label for="password" class="col-sm-3 hidden-md control-label">Type</label>
                        <div class="col-sm-9">
                          <select class="form-control" name="role_id" required>
                            <option value="2">General User</option>
                          </select>
                        </div>
                      </div><br><br>

                      <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                          <button type="submit" class="btn btn-default btn-sm" name="submit">Submit</button>
                          <a class="btn btn-default btn-sm" href="<?= url('/'); ?>">Go Home</a>
                        </div>
                      </div>

                    </form>
                </div>                
              </div>
          </div>
        </div>
    </div>

    <!-- footer -->
    <footer class="footer">
      <div class="container-fluid">
        <span class="text-muted">
          <strong>Copyright &copy; <?= date('Y'); ?>
        </span>
        <span class="pull-right"><b>Powered by</b> <a href="#">Solaman Badsha</a></span>
      </div>
    </footer>

  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="{{ asset('public/admin/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript">
        function checkPass(){
            var password = document.getElementById('password');
            var confirm_password = document.getElementById('confirm_password');
            var message = document.getElementById('confirmMessage');
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            if(password.value == confirm_password.value){
                confirm_password.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Password Match!"
            }else{
                confirm_password.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Password Does Not Match!"
            }
        }
    </script>
  </body>
</html>