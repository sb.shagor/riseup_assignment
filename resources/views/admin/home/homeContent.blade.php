@extends('admin.layout.default')
@section('title')
    Admin Dashboard
@endsection
@section('content')

    <div class="main-panel">
        <div class="page-header">
            <h3 class="page-title">
                Dashboard
            </h3>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            <i class="fas fa-calendar-alt"></i>
                            Calendar
                        </h4>
                        <div id="inline-datepicker-example" class="datepicker"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020-<?php echo date('Y'); ?></span>
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block pulli-right"></span>
            </div>
        </footer>
        <!-- partial -->
    </div>
@endsection