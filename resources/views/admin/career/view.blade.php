@extends('admin.layout.default')

@section('title')
	Manage Record
@endsection

@section('content')
	<div class="main-panel">
		<div class="page-header">
			<h3 class="page-title">All Record's List</h3>
		</div>
		<div class="content-wrapper">
			<div class="card">
				<div class="card-body">
					@if(Session::has('message'))
                        <div class="alert alert-block alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>
                            <i class="ace-icon fa fa-check green"></i>
                            {{ Session::get("message") }}
                            {{ Session::forget('message') }}
                        </div>
                    @endif
					<div class="row">
						<div class="col-12">
							<div class="table-responsive">
								<table id="order-listing" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>SN</th>
											<th>Title</th>
											<th>Description</th>
											<th>Status</th>
											<th>Thumbnail</th>
											<th>Details</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($get_all as $key => $value)
										<tr>
											<td class="center">{{ $key+1 }}</td>
											<td>{{ $value->title }}</td>
											<td>{{ limit_words(strip_tags($value->description),55,"UTF-8") }}
						                        <a href="#entry{{$value->id}}" role="button" class="btn btn-warning btn-xs" data-toggle="modal">Read More</a>
						                        <div id="entry{{$value->id}}" class="modal fade">
						                          <div class="modal-dialog">
						                            <div class="modal-content">
						                              <div class="modal-header">
						                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
						                              </div>
						                              <div class="modal-body">
						                                {!! $value->description !!}
						                              </div>
						                            </div>
						                          </div>
						                        </div>
						                    </td>
											<td>{{ ($value->status==1)?'Active':'Inactive' }}</td>
											<td>
												<a href="{{ asset($value->file) }}" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
											</td>
											<td>
												<a href="#" data-panel-id="{{ $value->id }}" onclick="viewDetails(this)" ><i class="fa fa-eye" aria-hidden="true"></i> View</a>
											</td>
											<td>
												@if(hasPermission("career",EDIT))
													<a href="{{ route('careers.edit',$value->id)}}" ><i class="fa fa-edit" aria-hidden="true"></i></a>
												@endif
                                                @if(hasPermission("career",DELETE))
												| <a href="{{ route('careers.destroy',$value->id) }}" onclick="return confirm('Are you sure ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="show_details">
                    
                   </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		function viewDetails(x){

        	$("#myModalLabel").html("Circular Details");
            btn = $(x).data('panel-id');
			$.ajax({
			    type:'get',
			    url:'{{ route("careers.circularDetails") }}',
			    data:{id:btn},
			    cache: false,
			    success:function(data) {
			        $('.modal-body').html(data);
			    }
			});

            $("#myModal").modal();
        }
	</script>

@endsection