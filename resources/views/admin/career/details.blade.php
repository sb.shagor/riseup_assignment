
<div class="row">
	<div class="col-12">
		<div class="widget-header">
			<h4 class="widget-title">{{ $selected_info->title }} </h4>
		</div>
		<hr>
		<div class="table-responsive">
			<center><img src="{{ asset($selected_info->file) }}" height="200px" width="220px"></center>
			<table id="order-listing" class="table table-striped table-bordered">
				<tbody>
					<tr>
						<td>Job Title</td>
						<td>{{ $selected_info->title }}</td>
					</tr>
					<tr>
						<td>Job Description</td>
						<td>{!! $selected_info->description !!}</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>{{ ($selected_info->status==1)?'Active':'Inactive' }}</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
</div>