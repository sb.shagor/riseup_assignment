@extends('admin.layout.default')
@section('title')
    Add Record
@endsection
@section('content')
    
<div class="main-panel">   
    <div class="page-header">
        <h3 class="page-title">Add New Record</h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form elements</li>
            </ol>
        </nav>
    </div>     
    <div class="content-wrapper">
        @isset($add)
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-block alert-success">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <i class="ace-icon fa fa-check green"></i>
                                    {{ Session::get("message") }}
                                    {{ Session::forget('message') }}
                                </div>
                            @endif

                            <form action="{{ route('careers.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                                
                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Title <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="title" class="form-control" placeholder="Enter title" required>
                                        <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Description <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea name="description" class="form-control" ></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Status <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" required class="form-control" >
                                            <option value="0">Please Select</option>
                                            <option value="1">Active</option>
                                            <option value="2">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="career" class="col-sm-3 col-form-label">Upload Thumbnail <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="file"  value="" required>
                                        <code>Max File Size less than 1MB</code>
                                        <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary mr-2">Save</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endisset
        @isset($edit)
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-block alert-success">
                                    <button type="button" class="close" data-dismiss="alert">
                                        <i class="ace-icon fa fa-times"></i>
                                    </button>
                                    <i class="ace-icon fa fa-check green"></i>
                                    {{ Session::get("message") }}
                                    {{ Session::forget('message') }}
                                </div>
                            @endif

                            <form action="{{ route('careers.update',$single->id) }}" method="post" name="editform" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                                
                                <div class="form-group row">
                                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Title <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="title" class="form-control" value="{{ $single->title }}" placeholder="Enter title" required>
                                        <span class="text-danger">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Description <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea name="description" class="form-control" >{{ $single->description }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Status <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="status" required class="form-control" >
                                            <option value="0">Please Select</option>
                                            <option value="1">Active</option>
                                            <option value="2">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="career" class="col-sm-3 col-form-label">Update Thumbnail <span style="color: red;font-size: 15px;">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="file"  value="">
                                        <a href="{{ asset($single->file) }}" target="_blank">View Existing Thumbnail</a>
                                        <code>Max File Size less than 1MB</code>
                                        <span class="text-danger">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="exampleInputMobile" class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <button type="submit" class="btn btn-primary mr-2">Save Changes</button>
                                        <button class="btn btn-light">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                document.forms['editform'].elements['status'].value=<?php echo $single->status; ?>
            </script>
        @endisset
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('description');
</script>
            
@endsection