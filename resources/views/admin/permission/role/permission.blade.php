@extends('admin.layout.default')
@section('title')
    Role Permission
@endsection
@section('content')

<div class="main-panel">
    <div class="page-header">
        <h3 class="page-title">Manage Permission</h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form elements</li>
            </ol>
        </nav>
    </div>
	
	<div class="content">
		<div class="container">
			@if(Session::has('message'))
                <div class="alert alert-block alert-{{Session::get('class')}}">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    {{ Session::get("message") }}
                    {{ Session::forget('message') }}
                </div>
            @endif
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<form action="{{ url('assign-permission/'.$role_id) }}" method="post" >
                        @csrf
							<table id="order-listing" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>Module Name</th>
										<th>Child Module Name</th>
										<th>Is View</th>
										<th>Is Add</th>
										<th>Is Edit</th>
										<th>Is Delete</th>
									</tr>
								</thead>
								<tbody>
									@if(count($permission_list)>0)
										@foreach($permission_list as $key=>$value)
											<tr>
												<th>
													{{$value['group_name']}}<br />
													<span class="m-l-15"><input value="{{$value['group_code']}}" class="group" type="checkbox"></span>
												</th>

												<td>
													<input type="hidden" name="per_cat[]" value="{{$value['permission'][0]->pc_id}}" />
													<input type="hidden" name="{{"roles_permissions_id_" . $value['permission'][0]->pc_id}}" value="{{$value['permission'][0]->rp_id}}" />
													{{$value['permission'][0]->name}}
												</td>
												
												<td>
													<label class="">
														<input class="group_{{$value['group_code']}}" type="checkbox" name="{{"can_view-perm_" . $value['permission'][0]->pc_id}}" value="{{$value['permission'][0]->pc_id}}" {{$value['permission'][0]->can_view == 1?"checked":""}}>
													</label>
												</td>

												<td>
													<label class="">
														<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_add-perm_" . $value['permission'][0]->pc_id}}" value="{{$value['permission'][0]->pc_id}}" {{$value['permission'][0]->can_add == 1?"checked":""}}>
													</label>
												</td>

												<td>
													<label class="">
														<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_edit-perm_" . $value['permission'][0]->pc_id}}" value="{{$value['permission'][0]->pc_id}}" {{$value['permission'][0]->can_edit == 1?"checked":""}}>
													</label>
												</td>

												<td>
													<label class="">
														<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_delete-perm_" . $value['permission'][0]->pc_id}}" value="{{$value['permission'][0]->pc_id}}" {{$value['permission'][0]->can_delete == 1?"checked":""}}>
													</label>
												</td>
											</tr>
												
											@if(!empty($value["permission"])&& count($value["permission"]) > 1)
												@unset($value["permission"][0])
												@foreach($value["permission"] as $new_feature_key => $new_feature_value)
													<tr>
														<td></td>
														<td>
															<input type="hidden" name="per_cat[]" value="{{$new_feature_value->pc_id}}" />
															<input type="hidden" name="{{"roles_permissions_id_" . $new_feature_value->pc_id}}" value="{{$new_feature_value->rp_id}}" />
															{{$new_feature_value->name}}
														</td>
														<td>
															@if( $new_feature_value->enable_view == 1 )
																<label class="">
																	<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_view-perm_" . $new_feature_value->pc_id}}" value="{{$new_feature_value->pc_id}}" {{$new_feature_value->can_view == 1?"checked":""}}>
																</label>
															@endif
														</td>

														<td>
															@if($new_feature_value->enable_add == 1 )
																<label class="">
																	<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_add-perm_" . $new_feature_value->pc_id}}" value="{{$new_feature_value->pc_id}}" {{$new_feature_value->can_add== 1?"checked":""}}>
																</label>
															@endif

														</td>

														<td>
															@if($new_feature_value->enable_edit == 1 )
															<label class="">
																<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_edit-perm_" . $new_feature_value->pc_id}}" value="{{$new_feature_value->pc_id}}" {{$new_feature_value->can_edit == 1?"checked":""}}>
															</label>
															@endif
														</td>

														<td>
															@if($new_feature_value->enable_delete == 1  )
															<label class="">
																<input type="checkbox" class="group_{{$value['group_code']}}" name="{{"can_delete-perm_" . $new_feature_value->pc_id}}" value="{{$new_feature_value->pc_id}}" {{$new_feature_value->can_delete == 1?"checked":""}}>
															</label>
															@endif
														</td>
													</tr>
												@endforeach
											@endif
										@endforeach
									@endif
								</tbody>
							</table>
							<div class="form-group">
								<input type="submit" class="btn-lg btn btn-primary pull-right" value="Save" name="submit" />
							</div>
						</form>
					</div>
				</div>
			</div> <!-- End row -->
		</div> <!-- container -->
	</div>
</div>
<script>
    $(".group").on("click",function(){
       var group_code=$(this).val();
        $('.group_'+group_code).not(this).prop('checked', this.checked);

    });
</script>
@endsection
