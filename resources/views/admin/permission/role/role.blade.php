@extends('admin.layout.default')
@section('title')
    Role
@endsection
@section('content')

<div class="main-panel">
    <div class="page-header">
        <h3 class="page-title">Manage Role</h3>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form elements</li>
            </ol>
        </nav>
    </div> 

    <div class="content">
        <div class="container">
            @if(Session::has('message'))
                <div class="alert alert-block alert-{{Session::get('class')}}">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    {{ Session::get("message") }}
                    {{ Session::forget('message') }}
                </div>
            @endif
            <div class="row">
                @isset($add)
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">

                                <form action="{{ url('role') }}" method="post" >
                                @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Role Name <span style="color: red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input required name="role_name" type="text" class="form-control" id="name" placeholder="Enter role name">
                                            <span class="text-danger">{{ $errors->has('role_name') ? $errors->first('role_name') : '' }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn-lg btn btn-primary pull-right" value="Save" name="submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endisset

                @isset($edit)
                    <div class="col-md-6 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">

                                <form action="{{ url('role') }}" method="post" >
                                @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-3 col-form-label">Role Name <span style="color: red;">*</span></label>
                                        <div class="col-sm-9">
                                            <input required name="role_name" type="text" class="form-control" id="name" value="{{$single->role_name}}" placeholder="Enter role name">
                                            <input type="hidden" value="{{$single->id}}" name="id" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn-lg btn btn-primary pull-right" value="Update" name="submit" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endisset

                <div class="col-sm-6">
                    <div class="table-responsive">
                        <table id="order-listing" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Role Name</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($role as $key => $value)
                                <tr>
                                    <td>{{ ++$key}}</td>
                                    <td>{{$value->role_name}}</td>
                                    <td>{{$value->type}}</td>
                                    <td>
                                        @if($value->role_name != 'Admin')
                                            <a data-toggle="tooltip" data-placement="top" title="Assign Permission!" href="{{url("assign-permission/".$value->id)}}" class="  btn-xs  waves-effect"><i class="fa fa-tag"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" title="Edit" href="{{url("role/edit/".$value->id)}}" class="  btn-xs  waves-effect"><i class="fa fa-edit"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" title="Delete" href="{{url("role/delete/".$value->id)}}" onclick="return confirm('Are you sure you want to delete this user?');" class="text-danger  btn-xs  waves-effect"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- End row -->
        </div> <!-- container -->
    </div>
</div> 

<script type="text/javascript">
	$(function () {
        $('[data-toggle="tooltip"]').tooltip();
	});
</script>

@endsection
