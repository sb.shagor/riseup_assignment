-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2021 at 05:16 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riseup`
--

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) NOT NULL,
  `description` text,
  `file` varchar(191) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `description`, `file`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Back-end Web Developer (PHP, Laravel)', '<p>Please read the whole JD before applying!!</p>\r\n\r\n<p>Back-end Web Developer (PHP, Laravel)<br />\r\nJob Type: Full time<br />\r\nNo. of Vacancies: 3<br />\r\nExperience: At least 2 year(s), &amp; Fresher candidates can apply, those who have good programming knowledge.</p>\r\n\r\n<p>Job Responsibilities:</p>\r\n\r\n<p>&bull; Develop PHP &amp; MySQL based services with or without using most popular frameworks;<br />\r\n&bull; Must have professional experience with Laravel;<br />\r\n&bull; Ability to build and consume RESTful web services;<br />\r\n&bull; Writing reusable, testable, and efficient code;<br />\r\n&bull; Understanding accessibility and security compliance;<br />\r\n&bull; Proficient understanding of JSON, AJAX, client-side scripting and JavaScript frameworks, including jQuery;<br />\r\n&bull; Proficient understanding of code versioning tools, such as Git.</p>\r\n\r\n<p>Educational Qualification:</p>\r\n\r\n<p>&bull; B.Sc in Computer Science and Engineering from any reputed public or private university.</p>\r\n\r\n<p>Additional Requirements:</p>\r\n\r\n<p>&bull; 2 years&#39; experience in web development;<br />\r\n&bull; Must have and able to provide online portfolio;<br />\r\n&bull; Should have experience working with live server.</p>\r\n\r\n<p>Workplace: Work at office</p>\r\n\r\n<p>Salary: Negotiable.</p>\r\n\r\n<p>Compensation &amp; Other Benefits:</p>\r\n\r\n<p>&bull; As per the company&#39;s policies</p>\r\n\r\n<p>NB: Only shortlisted candidates will be communicated in the recruitment process. (Email: hr@riseuplabs.com)</p>', 'career/career-1636559402.jpg', 1, 1, 1, '2021-11-10 01:59:24', '2021-11-10 09:50:02'),
(3, 'Flutter Developer', '<p>Please read the whole JD before applying!!</p>\r\n\r\n<p>Flutter Developer (<a href=\"https://riseuplabs.com/jobs/flutter-developer/?fbclid=IwAR01rxGKKTVNzTVIXzKY28jwJjxaZ4-0Z_32-zRz7d38lQyOWFpLzdFIDRA\" target=\"_blank\">https://riseuplabs.com/jobs/flutter-developer/</a>)<br />\r\nJob Type: Full time<br />\r\nNo. of Vacancies: 1<br />\r\nExperience: At least 1.5+ years of Flutter experience</p>\r\n\r\n<p>Job Responsibilities:<br />\r\n&bull; Working knowledge with Dart programming language;<br />\r\n&bull; Knowledge about flutter application lifecycle and state management;<br />\r\n&bull; Experience using making mobile apps using java/kotlin and swift/objective c is a plus;<br />\r\n&bull; Develop SDKs, libraries, and widgets to create a mobile application development platform;<br />\r\n&bull; Experience using version control systems like Git and CI/CD tools for mobile development;<br />\r\n&bull; Apply good software engineering principles and practices;<br />\r\n&bull; Familiarity with server communication and API Development (REST, Web API);<br />\r\n&bull; Design, document, and communicate solutions for improvement;<br />\r\n&bull; Work as part of a cross-functional team, which includes user experience researchers and designers, product managers, back-end engineers, and other functional specialists;<br />\r\n&bull; Must have at least 1 apps on play-store or app-store (Flutter).</p>\r\n\r\n<p>Educational Requirements:<br />\r\nB.Sc in Computer Science and Engineering from any reputed public or private university.</p>\r\n\r\n<p>Additional Requirements:<br />\r\n&bull; Good Documentation Skills (if any);<br />\r\n&bull; Working experiences in Native Android Development will be a plus.</p>\r\n\r\n<p>Workplace: Work at office.</p>\r\n\r\n<p>Salary: Negotiable.</p>\r\n\r\n<p>Compensation &amp; Other Benefits:</p>\r\n\r\n<p>&bull; As per the company&#39;s policies</p>\r\n\r\n<p>NB: Only shortlisted candidates will be communicated in the recruitment process. (Email: [Email hidden])</p>', 'career/career-1636559579.jpg', 1, 1, 1, '2021-11-10 02:37:36', '2021-11-10 09:52:59'),
(4, 'Cordova Developer', '<p>Please read the whole JD before applying!!</p>\r\n\r\n<p>Position: Cordova Developer<br />\r\nRiseup Labs<br />\r\nVacancy: 1<br />\r\nJob Nature: Full-time</p>\r\n\r\n<p>Job Responsibilities:<br />\r\n&bull; Having sound knowledge of Cordova development framework;<br />\r\n&bull; Capable enough to understand the requirement;<br />\r\n&bull; Strong hand on design, development and problem solving skills as well good debugging and analytical skill;<br />\r\n&bull; Ability to complete the assigned work accurately and meet deadlines with frequent interruptions;<br />\r\n&bull; Ability to work well independently as well as part of a team;<br />\r\n&bull; Daily work reporting;<br />\r\n&bull; Object Oriented Programming principles;<br />\r\n&bull; Cleanly written, well organized and commented code is key. Additional Skills Requirement;<br />\r\n&bull; Experience interacting with various API&#39;s;<br />\r\n&bull; Flexibility to work effectively in a changing environment;<br />\r\n&bull; May be required to perform other duties as requested, directed or assigned by the Reporting Person.</p>\r\n\r\n<p>Educational Requirements:<br />\r\n&bull; Bachelor of Science in Computer Science and Engineering.</p>\r\n\r\n<p>Additional Requirements:<br />\r\n&bull; Proficient command of English is must;<br />\r\n&bull; At least 3+ years of experience working as a Software Engineer (Cordova and AngularJS 1);<br />\r\n&bull; Published at least 3 apps on the aforementioned tech on both Android and iOS Stores.</p>\r\n\r\n<p>Salary: Negotiable/ Handsome salary will be offered for deserving candidate.</p>\r\n\r\n<p>Compensation &amp; other benefits: As per the company&#39;s policies.</p>\r\n\r\n<p>N.B.: The candidate who fulfill the above requirements may submit their application. Please do not apply unnecessarily if it does not match with your profile. (Email: hr@riseuplabs.com)</p>', 'career/career-1636559714.jpg', 1, 1, NULL, '2021-11-10 09:55:14', '2021-11-10 09:55:14'),
(5, 'Java Developer', '<p>Java Developer<br />\r\nJob Type: Full Time<br />\r\nNo. of Vacancies: 2<br />\r\nExperience: At least 3 years</p>\r\n\r\n<p>Responsibilities:</p>\r\n\r\n<p>&bull; Work with business users to gather functional requirements;<br />\r\n&bull; Combine your technical expertise and problem-solving passion to turn complex problems into end-to-end solutions;<br />\r\n&bull; Work with client architect/senior developers to do high level/low level design/architecture;<br />\r\n&bull; Design and implement high-quality, test-driven BE code for various projects;<br />\r\n&bull; Unit Testing/Integration Testing;<br />\r\n&bull; Code Configuration and Release Management;<br />\r\n&bull; Create and maintain documentation, implement and follow best practices for development workflow;<br />\r\n&bull; Work collaboratively with team members to ensure deadlines are met;<br />\r\n&bull; Stay current on changes in technology and keep adding to your skillset.</p>\r\n\r\n<p>Educational Requirements:</p>\r\n\r\n<p>B.Sc in Computer Science and Engineering from any reputed public &amp; private university.</p>\r\n\r\n<p>Additional Requirements</p>\r\n\r\n<p>&bull; Minimum 3 Years of experience in Web Application and API development in Java 8 and above;<br />\r\n&bull; Working experience with MVC frameworks like Spring, Play, etc;<br />\r\n&bull; Experience with Multi-threading, Collections, and concurrent API;<br />\r\n&bull; Working experience with web-services and APIs (REST, SOAP);<br />\r\n&bull; Working experience with data platforms (relational and/or NoSQL) and messaging technologies;<br />\r\n&bull; Excellent OOPs, data structure, and algorithm knowledge;<br />\r\n&bull; Understanding &amp; experience in API management, Swagger;<br />\r\n&bull; Working knowledge of API Testing Tools (e.g. Postman), Version control systems like GIT;<br />\r\n&bull; Working experience with LINUX/UNIX environment and shell scripts;<br />\r\n&bull; Proficiency in English;<br />\r\n&bull; Strong collaborator and comfortable to work in an agile, remote and distributed team environment.</p>\r\n\r\n<p>Nice to have:</p>\r\n\r\n<p>&bull; Experience in one or more front-end development technologies;<br />\r\n&bull; Experience in developing microservices in Spring Boot;<br />\r\n&bull; Experience writing high-quality code with fully automated unit test coverage (Junit, Mockito, etc.);<br />\r\n&bull; Experience defining and applying design/coding standards, patterns, and quality metrics depending on the solution;<br />\r\n&bull; Working experience with various CI/CD systems (Jenkins, Docker, Kubernetes) and build tools (ant, maven, gradle, etc.);<br />\r\n&bull; Working experience creating high performing applications, including profiling and tuning to improve performance;<br />\r\n&bull; Experience in Scrum/Agile;<br />\r\n&bull; Knowledge of public cloud infrastructures (AWS, Azure, GCP);<br />\r\n&bull; Knowledge of one or more security or integration framework (PING, Octa).</p>\r\n\r\n<p>Workplace: Work at office</p>\r\n\r\n<p>Joining Date: Immediately</p>\r\n\r\n<p>Salary Range: Negotiable</p>\r\n\r\n<p>Compensation &amp; Other Benefits:</p>\r\n\r\n<p>&bull; As per the company&#39;s policies.</p>\r\n\r\n<p>The candidate who fulfill the above requirements may submit their application. Please do not apply unnecessarily if it does not match with your profile. Email to hr@riseuplabs.com</p>', 'career/career-1636559775.jpg', 1, 1, NULL, '2021-11-10 09:56:15', '2021-11-10 09:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_19_040502_create_role_permissions_table', 1),
(5, '2020_09_06_064224_create_suppliers_table', 1),
(6, '2020_09_26_061536_create_buyers_table', 2),
(7, '2020_09_27_043948_create_items_table', 3),
(8, '2020_09_27_044228_create_grades_table', 3),
(9, '2020_09_29_051344_create_bank_accounts_table', 4),
(10, '2020_09_30_091528_create_modules_table', 5),
(11, '2020_09_30_100024_create_role_permissions_table', 6),
(12, '2020_12_30_070445_create_orders_table', 7),
(13, '2020_12_30_070857_create_order_items_table', 7),
(14, '2020_12_31_071137_create_lc_dates_table', 8),
(15, '2020_12_31_071432_create_shipments_table', 8),
(16, '2021_01_07_110000_create_expense_types_table', 9),
(17, '2021_01_10_091827_create_system_settings_table', 10),
(18, '2021_01_10_105700_create_account_settings_table', 11),
(19, '2021_01_13_060705_create_expenses_table', 12),
(20, '2021_01_16_081508_create_bank_transactions_table', 13),
(21, '2021_01_20_105515_create_payment_receives_table', 14),
(22, '2021_01_21_052706_create_make_payments_table', 15),
(23, '2021_02_11_063348_create_shipment_schedules_table', 16),
(24, '2021_02_11_092921_create_revised_schedules_table', 17),
(25, '2021_11_10_065209_create_careers_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_categories`
--

CREATE TABLE `permission_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission_group_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `submenu` tinyint(1) NOT NULL DEFAULT '0',
  `subparent` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` smallint(6) DEFAULT NULL,
  `enable_view` tinyint(1) NOT NULL DEFAULT '1',
  `enable_add` tinyint(1) NOT NULL DEFAULT '1',
  `enable_edit` tinyint(1) NOT NULL DEFAULT '1',
  `enable_delete` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_categories`
--

INSERT INTO `permission_categories` (`id`, `permission_group_id`, `name`, `short_code`, `link`, `submenu`, `subparent`, `icon`, `position`, `enable_view`, `enable_add`, `enable_edit`, `enable_delete`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dashboard', 'dashboard', 'dashboard', 0, 0, NULL, 1, 1, 1, 1, 1, 1, NULL, NULL),
(3, 3, 'Career', 'career', 'career', 0, 0, NULL, 4, 1, 1, 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` smallint(6) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_groups`
--

INSERT INTO `permission_groups` (`id`, `name`, `short_code`, `link`, `position`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'dashboard', 'dashboard', 1, 1, NULL, NULL),
(3, 'Career', 'career', 'career', 3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'custom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'system', '2021-04-08 17:01:14', '2021-04-12 14:22:12'),
(2, 'General User', 'custom', '2021-04-08 17:01:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_category_id` int(11) NOT NULL,
  `can_view` tinyint(1) NOT NULL DEFAULT '0',
  `can_add` tinyint(1) NOT NULL DEFAULT '0',
  `can_edit` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_category_id`, `can_view`, `can_add`, `can_edit`, `can_delete`, `created_at`, `updated_at`) VALUES
(3, 2, 2, 1, 1, 1, 0, NULL, NULL),
(15, 2, 3, 1, 0, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8 NOT NULL,
  `password` varchar(191) CHARACTER SET utf8 NOT NULL COMMENT '123456',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `image` mediumtext CHARACTER SET utf8,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `role_id`, `email`, `password`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Solaman Badsha Shagor', 1, 'shagor.ahmed374@gmail.com', '$2y$10$XFR80XRwTjdnwPKfA.bqlOKGQSFhZPd/EVtVHu/FIIagSPrYgHbAa', 1, 'userImage/me2.jpg', '2020-09-27 11:16:35', '2020-09-27 11:16:35'),
(4, 'General User', 2, 'general@example.com', '$2y$10$XFR80XRwTjdnwPKfA.bqlOKGQSFhZPd/EVtVHu/FIIagSPrYgHbAa', 1, 'userImage/111.jpg', '2021-04-12 10:12:50', '2021-04-12 10:12:50'),
(8, 'Solaman-GU', 2, 'gu@gmail.com', '$2y$10$2dfQFrEl1W5HJOJ/VBT55O0G52qTR9w9f0E7tAIZ/JQbCviUmMoeW', 1, NULL, '2021-11-10 10:21:23', '2021-11-10 10:21:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permission_categories`
--
ALTER TABLE `permission_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_categories_name_unique` (`name`),
  ADD UNIQUE KEY `permission_categories_short_code_unique` (`short_code`),
  ADD UNIQUE KEY `permission_categories_link_unique` (`link`),
  ADD KEY `permission_categories_permission_group_id_index` (`permission_group_id`),
  ADD KEY `permission_categories_status_index` (`status`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`),
  ADD UNIQUE KEY `permission_groups_short_code_unique` (`short_code`),
  ADD UNIQUE KEY `permission_groups_link_unique` (`link`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permission_categories`
--
ALTER TABLE `permission_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
